# -*- coding: utf-8 -*-
{
    'name': "Contact",
    'version': '13.0.0.1',
    'category':"Extra tools",
    'summary': """Manging the Contact""",
    'sequence':'-1',
    "maintener":"Sunanda",

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/contact.xml',
        'views/templates.xml',
    ],
    'installable':True,
    'application':True,
    'auto_install':False,
    
}
