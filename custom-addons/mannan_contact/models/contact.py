from odoo import models, fields


class ContactManagement(models.Model):
    _name = 'contact.management'
    _description = "Contact Record"
    _rec_name='customer_name'
    # company_type = fields.Selection(string='Company Type',
    #                                 selection=[('person', 'Individual'), ('company', 'Company'),('specifier', 'Specifier')],
    #                                )
    customer_name = fields.Char(string='Customer', required=True)
    image = fields.Binary(string="Image")
    phone = fields.Char()
    mobile = fields.Char()
    image=fields.Binary(string="Image",attachement=True)
    street = fields.Char('Company Address')
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    specifier = fields.Many2one('contact.management')
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict',
                               domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
    
    partner_latitude = fields.Float(string='Geo Latitude', digits=(16, 5))
    partner_longitude = fields.Float(string='Geo Longitude', digits=(16, 5))
    email = fields.Char()
    contact_address = fields.Char(compute='_compute_contact_address', string='Complete Address')
